//
//  ViewController.swift
//  FaceIt
//
//  Created by Milan on 8/23/17.
//  Copyright © 2017 Milan. All rights reserved.
//
import UIKit

class FaceViewController: VCLLoggingViewController {
    
    //MARK: - Variables
    var expression = FacialExpression(eyes: .open, mouth: .smile) {
        didSet {
            updateUI() }
    }
    
    //MARK: - Constants
    private let mouthCurvatures = [FacialExpression.Mouth.grin:0.5,.frown:-1.0,.smile:1.0,.neutral:0.0,.smirk:-0.5]
    
    //MARK: - Outlets
    @IBOutlet weak var faceView: FaceView! {
        
        didSet {
            
            updateUI()
        }
    }
    
    //MARK: - UI update
    private func updateUI() {
        
        switch expression.eyes {
            
        case .open:
            faceView?.eyesOpen = true
        case .closed:
            faceView?.eyesOpen = false
        case .squinting:
            faceView?.eyesOpen = false
        }
        
        faceView?.mouthCurvature = mouthCurvatures[expression.mouth] ?? 0.0
    }
    
    //MARK: - Init of gestures
    private func initGestures() {
        
        initSwipeUpGesture()
        initSwipeDownGesture()
        initPinchGesture()
        initTapGesture()
    }
    
    private func initSwipeDownGesture() {
        
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(decreaseHappiness))
        swipeDownRecognizer.direction = .down
        faceView.addGestureRecognizer(swipeDownRecognizer)
    }
    
    private func initSwipeUpGesture() {
        
        let swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(increaseHappiness))
        swipeUpRecognizer.direction = .up
        faceView.addGestureRecognizer(swipeUpRecognizer)

    }
    
    private func initPinchGesture() {
        
        let pinchRecognizer = UIPinchGestureRecognizer(target: faceView, action: #selector(FaceView.changeScale(byReactingTo:)))
        faceView.addGestureRecognizer(pinchRecognizer)    }
    
    private func initTapGesture() {
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleEyes(byReactingTo:)))
        tapRecognizer.numberOfTapsRequired = 1
        faceView.addGestureRecognizer(tapRecognizer)
    }
    
    //MARK: - Set happiness
    func increaseHappiness() {
        expression = expression.happier
    }
    
    func decreaseHappiness() {
        expression = expression.sadder
    }
    
    func toggleEyes(byReactingTo tapRecognizer: UITapGestureRecognizer) {
        
        if tapRecognizer.state == .ended {
            
            let eyes: FacialExpression.Eyes = (expression.eyes == .closed) ? .open : .closed
            expression = FacialExpression(eyes: eyes, mouth: expression.mouth)
        }
    }
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        initGestures()
    }
}
